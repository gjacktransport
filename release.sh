#/bin/sh
#
# updated VERSION in 
# configure.in, debian/changelog, ChangeLog
#

sh autogen.sh
make dist

VERSION=$(awk '/define VERSION /{print $3;}' config.h | sed 's/"//g')

if [ -z "$VERSION" ]; then 
  echo "unknown VERSION number"
  exit 1;
fi

MVERSION=$(echo $VERSION | sed 's/\.[0-9]*$//')
echo "VERSION: $VERSION -> v$MVERSION"

echo -n "OK? [enter|ctrl-c]"; read;

git commit -a
git tag "v$VERSION" || (echo -n "version tagging failed. - press Enter to continue, CTRL-C to stop."; read; )
# upload to rg42.org git
git push
git push --tags
# upload to sourceforge git
git push sf
git push --tags sf


if true; then
echo upload files to sourceforge
sftp x42,gjacktransport@frs.sourceforge.net << EOF
cd /home/frs/project/g/gj/gjacktransport/gjacktransport
mkdir v${MVERSION}
cd v${MVERSION}
put gjacktransport-${VERSION}.tar.gz
EOF
fi

if false; then
echo upload doc to sourceforge
sftp x42,gjacktransport@web.sourceforge.net << EOF
cd htdocs/
rm *
lcd www
put -P *
chmod 0664 *.*
EOF
fi
