#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#include "gjack.h"
#include <jack/jack.h>
#include <jack/transport.h>
#include "framerate.h"


/* status bar */

#include <gtk/gtk.h>
#include "support.h"
extern GtkWidget *window1;
void dothejack (gboolean onoff);

void status_msg (char *msg) {
	gchar buf[PATH_MAX];
	guint context = 1;
	GtkWidget *statusbar1;
	statusbar1=lookup_widget(window1,"statusbar1");

	snprintf(buf, PATH_MAX, "%s", msg);
	gtk_statusbar_pop(GTK_STATUSBAR(statusbar1), context);
	gtk_statusbar_push(GTK_STATUSBAR(statusbar1), context,  buf);
}


void status_printf (const char *format, ...) {
	va_list arglist;
	char text[PATH_MAX];

	va_start(arglist, format);
	vsnprintf(text, PATH_MAX, format, arglist);
	va_end(arglist);
	text[PATH_MAX -1] =0;
	
	status_msg(text);
}

/* end status bar */



jack_client_t *jack_client = NULL;
jack_nframes_t j_srate = 48000;
char jackid[16];
int j_have_vfps = 0;

extern FrameRate *uffr;
extern FrameRate *jffr;

/* when jack shuts down... */
void jack_shutdown(void *arg)
{
	jack_client=NULL;
	status_printf ("jack server shutdown");
	dothejack(0); //< toggle connected button...
}

extern gdouble unit_table[UNIT_LAST];
/* when jack changes its samplerate ... */
int jack_srate_callback(jack_nframes_t nframes, void *arg)
{
        status_printf("jack sample rate: %i samples per second.",nframes);
	j_srate = nframes;
	// NOTE: this overrides the factor in the table.
	// we'll be  in trouble... ( oldunit == newunit)
	// if it changes while the spinbutton's value is displayed in Audioframe units.
	// but it will most probably never change anyway..
	unit_table[UNIT_AUDIOFRAMES] = (gdouble) 1.0/j_srate;
	return(0);
}


int jack_connected(void)
{
	if (jack_client) return (1);
	return (0);
}

void open_jack(void ) 
{
	if (jack_client) {
		status_printf ("already connected to jack..");
		return;
	}

	int i = 0;
	do {
		snprintf(jackid,16,"gjtctl-%i",i);
		jack_client = jack_client_open (jackid, JackUseExactName, NULL);
	} while (jack_client == 0 && i++<16);

	if (!jack_client) {
		status_printf("could not connect to jack server.");
	} else { 
		jack_on_shutdown (jack_client, jack_shutdown, 0);
		jack_set_sample_rate_callback (jack_client, jack_srate_callback, NULL);
		status_printf("connected as jack client '%s'",jackid);
		jack_srate_callback(jack_get_sample_rate(jack_client),NULL); // init j_srate
		jack_activate (jack_client);
	}
}

void close_jack(void)
{
	if (jack_client) {
		jack_deactivate (jack_client);
		jack_client_close (jack_client);
		status_printf("disconnected from jack.");
	}
	jack_client=NULL;
}

double jack_poll_time (void) 
{
	jack_position_t	jack_position;
	double		jack_time;

	if (!jack_client) { 
		j_have_vfps=0;
		return (-1);
	}

	/* Calculate frame. */
	jack_transport_query(jack_client, &jack_position);
	jack_time = jack_position.frame / (double) jack_position.frame_rate;
	FR_setsamplerate(jffr,j_srate);

	if (jack_position.valid&JackAudioVideoRatio) {
		double jfps= jack_position.frame_rate / (double) jack_position.audio_frames_per_video_frame;
		FR_setdbl(jffr,jfps,1);
		j_have_vfps=1;
	} else {
		j_have_vfps=0;
	}

	return(jack_time);
}


void jack_reposition(double sec)
{
	jack_nframes_t frame= rint(j_srate * sec);
	if (jack_client)
		jack_transport_locate (jack_client, frame);
}

void jack_stop(void)
{
	if (jack_client) {
		jack_transport_stop (jack_client);
		status_printf("paused jack transport.");
	}
}

void jack_play(void)
{
	if (jack_client) {
		jack_transport_start (jack_client);
		status_printf("jack transport rolling.");
	}
}

int jack_poll_transport_state (void) {
	if (jack_client) 
		switch (jack_transport_query(jack_client, NULL)) {
			case JackTransportRolling:
				return 1|4;
			case JackTransportStopped:
				return 2|4;
			default:
				return 3|4;
		}
	return 0;
}
