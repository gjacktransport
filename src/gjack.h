
enum {  UNIT_HOUR = 0 , UNIT_MIN, UNIT_SEC, UNIT_AUDIOFRAMES };
#define UNIT_LAST 15

int jack_connected(void);
void open_jack(void );
void close_jack(void);
double jack_poll_time (void);
int jack_poll_transport_state (void);
void jack_reposition(double sec);
void jack_play(void);
void jack_stop(void);

void status_printf (const char *format, ...);
