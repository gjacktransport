/* gjacktransport - jack transport slider
 *  Copyright (C) 2006 Robin Gareus  <robin AT gareus.org>
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#ifdef HAVE_CONFIG_H
# include <config.h>
#endif

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <gtk/gtk.h>
#include <getopt.h>
#include <libintl.h>

#include "interface.h"
#include "support.h"
#include "gjack.h"
#include "gjacktransport.h"
#include "framerate.h"
#include "callbacks.h"

char *program_name;
GtkWidget *window1;
FrameRate *jffr; //< jack video FPS
FrameRate *uffr; //< user set FPS
FrameRate *fffr; //< pointer to currently used.

gjtdata *stg; // settings
int mem = 0; // current mem
double stride = 10; // seek stride (in seconds)
int want_quiet = 0;
int no_lash = 0; // don't try to use LASH
int max_mem = 6;
int setup_done = 0;
char *smpte_font = NULL;
char *rc_file = NULL;

#ifdef HAVE_LASH
# include <lash/lash.h>
void lash_setup(void); //< prototype - fn defined in lash.c
lash_client_t *lash_client = NULL;
#endif
void lash_process(void);

// prototypes - defined in callbacks.c
gboolean  gpolljack (gpointer data);
void initialize_gjt (void);
void update_spinbtns(void);
void update_combotxt (int pos);
gboolean limit_height (gpointer data);
void post_rc_update (void);

// prototypes - defined in resourceconfig.c
void load_rc (void);
void try_load_rc_file (char *filename);

static GdkPixmap *gjt_icon_pixmap;
static GdkBitmap *gjt_icon_mask;
static int got_sigusr1 = 0;
#include "gjt_icon.xpm"


static void usage (int status) {
  printf("%s - JACK transport slider.\n",program_name);
  printf("usage: %s [Options] [start-marker [end-marker]]\n",program_name);
 printf (""
"Options:\n"
"  -h, --help                display this help and exit\n"
"  -l <file>,\n"
"    --config <file>         load configuration from <file>\n"
#ifdef HAVE_LASH
"  -L, --nolash              ignore the fact that gjt could use LASH.\n"
"  --lash-no-autoresume      [liblash option]\n"
"  --lash-no-start-server    [liblash option]\n"
#endif
"  -q, --quiet, --silent     inhibit usual output\n"
"  -V, --version             print version information and exit\n"
""
"unit for start/end markers: seconds. (eg 10 60.5)\n"
);
  exit(status);
}

static void printversion (void) {
  printf ("gjacktransport %s\n (C) GPL 2007, 2010 Robin Gareus <robin@gareus.org>\n", VERSION);
}

gboolean glashproc (gpointer data) {
  lash_process();
  return(TRUE);
}


static struct option const long_options[] =
{
  {"help", no_argument, 0, 'h'},
  {"quiet", no_argument, 0, 'q'},
  {"silent", no_argument, 0, 'q'},
  {"version", no_argument, 0, 'V'},
  {"nolash", no_argument, 0, 'L'},
  {"config", required_argument, 0, 'l'},
  {NULL, 0, NULL, 0}
};


static int
decode_switches (int argc, char **argv)
{
  int c;
  while ((c = getopt_long (argc, argv, 
                           "q"  /* quiet or silent */
                           "L"  /* no lash */
                           "h"  /* help */
                           "l:" /* config file */
                           "V", /* version */
                           long_options, (int *) 0)) != EOF)
  {
    switch (c) {
        case 'q':               /* --quiet, --silent */
          want_quiet = 1;
          break;
        case 'L':               /* --nolash */
          no_lash = 1;
          break;
        case 'l':
          if (rc_file) free(rc_file);
          rc_file = strdup(optarg);
          break;
        case 'V':
          printversion();
          exit(0);
        case 'h':
          usage (0);
        default:
          usage (EXIT_FAILURE);
    }
  }
  return optind;
}

void receive_signal(int sig)
{
  if(sig == SIGUSR1)
    got_sigusr1 = 1;
}

int install_signal_handlers(void)
{
  /* install signal handlers */
  struct sigaction action;
  memset(&action, 0, sizeof(action));
  action.sa_handler = receive_signal;

  if(sigaction(SIGUSR1, &action, NULL) == -1)
  {
    perror("sigaction() failed");
    return -1;
  }

  return 0;
}

gboolean handle_signals (gpointer data)
{
  if(got_sigusr1 == 1)
  {
    on_button_saverc_released(NULL, NULL);
    got_sigusr1 = 0;
  }
  return (TRUE);
}

int main (int argc, char *argv[]) {
  GtkWidget *w;
  program_name = argv[0];

  if(install_signal_handlers() == -1)
    printf("Signal handlers not installed!");

#ifdef ENABLE_NLS
  bindtextdomain (GETTEXT_PACKAGE, PACKAGE_LOCALE_DIR);
  bind_textdomain_codeset (GETTEXT_PACKAGE, "UTF-8");
  textdomain (GETTEXT_PACKAGE);
#endif

  gtk_init(&argc, &argv);
#ifdef HAVE_LASH
  lash_args_t *lash_args = lash_extract_args(&argc, &argv);
#endif

  initialize_gjt();

  int i = decode_switches (argc, argv);
  if (argc > i+2 ) usage(1);

  window1 = create_window1();
 
  w = lookup_widget(window1,"combobox1");
  gtk_combo_box_set_active (GTK_COMBO_BOX(w),6);
  w = lookup_widget(window1,"combobox2");
  gtk_combo_box_set_active (GTK_COMBO_BOX(w),2);
  w = lookup_widget(window1,"combobox3");
  gtk_combo_box_set_active (GTK_COMBO_BOX(w),0);
  w = lookup_widget(window1,"combobox4");
  gtk_combo_box_set_active (GTK_COMBO_BOX(w),0);
  w = lookup_widget(window1,"combobox5");
  gtk_combo_box_set_active (GTK_COMBO_BOX(w),0);

  load_rc();
  if(rc_file) try_load_rc_file(rc_file);

  if (argc-i == 2) {
    /* set start end end marker */
    mem=0;
    stg[0].unit=UNIT_SEC;
    stg[0].start=strtod(argv[i],NULL); //* unit_table[stg[0].unit];
    stg[0].end=strtod(argv[i+1],NULL); //* unit_table[stg[0].unit];
  } else if (argc-i == 1) {
    /* set end marker only */
    mem=0;
    stg[0].unit=UNIT_SEC;
    stg[0].end=strtod(argv[i],NULL);; //* unit_table[stg[0].unit];
  }

  post_rc_update();

#ifdef HAVE_LASH
  if (!no_lash) {
    lash_client = lash_init (lash_args, PACKAGE_NAME, 0  | LASH_Config_Data_Set ,LASH_PROTOCOL (2,0));
    if (!lash_client) {
      fprintf(stderr, "could not initialise LASH!\n");
    } else { 
      lash_setup();
      if (!want_quiet) printf("Connected to LASH.\n");
    }
    //lash_args_destroy(lash_args);
  }
#endif
  setup_done=1;

  gtk_widget_show (window1);
  gjt_icon_pixmap = gdk_pixmap_create_from_xpm_d (window1->window, &gjt_icon_mask, NULL, gjt_icon_xpm);

  gdk_window_set_icon(window1->window, NULL, gjt_icon_pixmap, gjt_icon_mask);
  //gdk_pixbuf_unref (window1_icon_pixbuf);
  gtk_idle_add(limit_height, NULL);
  g_timeout_add(40,gpolljack,NULL);
#ifdef HAVE_LASH
  if (!no_lash) {
    g_timeout_add(250,glashproc,NULL);
  }
#endif
  g_timeout_add(250, handle_signals, NULL);
  gtk_main ();

  FR_free(uffr);
  FR_free(jffr);
  for (i=0;i<max_mem;i++){
    if (stg[i].name) free(stg[i].name);
  }
  free(stg);
  if (smpte_font) free(smpte_font);
  if (rc_file) free(rc_file);
  return 0;
}
/* vi:set ts=8 sw=2: */ 
